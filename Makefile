FILE = main
SOURCE_DIR = latex
OUTPUT_DIR = output
FLAG = -halt-on-error# -quiet



all: $(FILE).pdf
	firefox $< &

$(FILE).pdf: compilation
	@echo "Compilation completed."
	@mv $(OUTPUT_DIR)/$(FILE).pdf $@

compilation: $(FILE).tex
	pdflatex $(FLAG) -output-directory=$(OUTPUT_DIR) $(SOURCE_DIR)/$<
	pdflatex $(FLAG) -output-directory=$(OUTPUT_DIR) $(SOURCE_DIR)/$<

glossaire : $(FILE)
	makeglossaries -d $(OUTPUT) $<

clean:
	rm -rf $(OUTPUT_DIR)/*
	mkdir -p $(OUTPUT_DIR)

help : ## affiche cette aide
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "} \
		{printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

.PHONY: all $(FILE).pdf clean help


