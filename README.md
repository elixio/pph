
# Projet Personel et Humanitaire

Projet de cours de l'INSA

## Dépendences

`sudo apt update && snap install codium --classic && sudo apt install make texlive-latex-base texlive-latex-extra texlive-lang-french`

## Compiler

- installer texlive
- installer texlive-lagfrench
- `make all -j4` : compile le projet
- `make help` : affiche l'aide du Makefile



