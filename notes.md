
## liens

[moodle étapes](https://moodle.insa-lyon.fr/course/view.php?id=8140)

[moodle consignes](https://moodle.insa-lyon.fr/course/view.php?id=5023)

## mth

- Pouvez vous exposer en quelques lignes en quoi consiste le thème de votre PPH, quel est votre principal axe de questionnement ?
- Quel est le point de départ personnel de votre réflexion ? Quels sont le ou les problèmes concrets qui vous on sensibilisé à cette question ?
- Quelle est votre démarche d'investigation ? Quel sera votre cheminement de réflexion ? Quels moyens comptez-vous mettre en oeuvre ?
- Quelles sont les premières références documentaires que vous compter exploiter (quels auteurs, quelles oeuvres ou quels documents souhaitez-vous vous appuyer) ?

## etapes

- [x] tuteur
- [x] signer anti-plagiat
- [x] question : 
- [x] motivation : Je m'intéresse depuis peu à la communication et la vulgarisation.
- [x] démarche : 
- [x] forme : 
- [x] tag : enseignement, communication, vulgarisation, débattre, biais cognitifs, ...
- [x] sujet : 
- [x] article scientifique (1 ressource min)
- [x] livre (1 ressource min)
- [x] 5 ressources au total (référencer selon la [norme](https://referencesbibliographiques.insa-lyon.fr/sites/referencesbibliographiques.insa-lyon.fr/files/u20/infographie_refbiblio_cc_2019.pdf) [générateur](https://www.scribbr.fr/references/generateur/apa/))
- [x] faire entretien
- [x] remplir et signer fiche contrat
- [ ] (e5) trouver second membre de jury de soutenance
- [ ] (e5) trouver date de soutenance
- [ ] (e6) déposer rapport soutenance
- [ ] (e6) déposer ficher d'évalution du jury
- [ ] (e7) déposer rapport final

## ressources

points intéressants par ressource :

- [TED Why do we ask questions](https://www.youtube.com/watch?v=u9hauSrihYQ)
- [TED The super mario effect](https://www.youtube.com/watch?v=9vJRopau0g0)
- [yt débattre chat sceptique](https://www.youtube.com/watch?v=ohU1tEwxOSE)
	- présentation cible de graham
- [TED How to speak so that people want to listen](https://www.youtube.com/watch?v=eIho2S0ZahI)
- [TED Teach teachers how to create magic](https://www.youtube.com/watch?v=H3ddtbeduoo)
	- skill d'oral possible -> voir église noir, barbier, ...
- [TED 10 ways to have a better conversation](https://www.youtube.com/watch?v=R1vskiVDwl4)
- [TED Do schools kill creativity?](https://www.youtube.com/watch?v=iG9CE55wbtY)
- [TED What makes a good teacher great?](https://www.youtube.com/watch?v=vrU6YJle6Q4)
- [TED Teaching Methods for Inspiring the Students of the Future](https://www.youtube.com/watch?v=UCFg9bcW7Bk)
- [TED The One Thing All Great Teachers Do](https://www.youtube.com/watch?v=WwTpfVQgkU0)
- Book - We Need to Talk: How to Have Conversations That Matter
- Book - Unleash the Power of Storytelling: Win Hearts, Change Minds, Get Results
- [Chaine yt sur la vulgarisation](https://www.youtube.com/@SciPlus/videos)
- Book - Pour un enseignment stratégique : l'apport de la psychologie cognitive, Jacques Tardif
- Book - Aliénation et accélération, Hartmut Rosa
- Article - Karsenti, T. (2018). Intelligence artificielle en éducation : L'urgence de préparer les futurs enseignants aujourd'hui pour l'école de demain ? Formation et Profession, 26(3), 112. https://doi.org/10.18162/fp.2018.a159
- Book - Communiquer efficacement pour les nuls
- Ma thèse en 360s
- Article - Favoriser l'agentivité en matière d'intégration de l'intelligence artificielle en contexte scolaire : Cinq stratégies à déployer dans son milieu | WorldCat.org. (2023). https://search.worldcat.org/title/10116266569
- https://eduq.info/xmlui/bitstream/handle/11515/21265/langevin_04_1_1.pdf?sequence=1
- https://direct.mit.edu/rest/article-abstract/doi/10.1162/rest_a_01328/115633/Super-Mario-Meets-AI-Experimental-Effects-of

## envies

- oral : mettre des questions régulièrement pour recaptiver l'attention
- 
- 

## outils

- [transcript](https://anthiago.com/transcript/)
- 

## themes

- constat : perte d'attention des nouvelles générations
- importance de la forme dans la communication
	- méthodes pour maintenir l'attention
	- ia dans l'apprentissage
- fond
	- biais
	- sophismes
- vulgarisation (avantages et inconvénients)

- comment être clair dans ses explication ?






etudes : dynamiques d'attentions, 

les gens sont de moins en moins patients --> comment apprendre dans ces cas là ?

Hartmut rosa : accélération et aliénation

les différents avantages et inconvénients des supports numériques



aller au delà de ce que l'IA sait faire (calcul mental utile ?)

nuance : fiabilité du résultat de chatGPT vs calculette



domaine des sciences cognitives sciences de l'éducation

**bmc** (recherche avancée)
cairn.info (log insa)
worldcat
google scolar
bibliothèque diderot lyon
peb











FALC : Facile A Lire et à Comprendre -> voir les critères


accessibilité








Pourquoi les questions comme celle-ci sont si prisées (dev personnel...) ?




## oral

(source)(https://www.youtube.com/watch?v=aXerxY1Dupo)

1. amorce (anecdote/citation/question)
1. prestation : envie de partager (préparé avec soin mais servit avec naturel)
1. présenter le sujet
1. vécu (présenter votre point de vue)
1. analogies/comparaisons
1. humour (subtil : sourire puis réfléchir)
1. résumer/fin d'histoire/ouverture






- Tous les marketeurs racontent des histoires seth godin
- [Bonne vulgarisation scientifique](https://www.youtube.com/watch?v=gJU3lR4AQVc)
- [e-penser x jammy : la vulgarisation](https://www.youtube.com/watch?v=w9OhqgY1JGc)
- etienne klein - 
- trou noir et distorsion du temps - kip thorne




**examplarité comme modèle d'apprentissage**

comment résoudre un conflit ?

**is statement/ought statement**







poster models :
- [ait ichou](file:///home/gasp/Documents/ecole/assos_other/AEDI/RDD/Posters2022-2024/AIT_ICHOU_Yoann_Poster_PFE_2023[RetD].pdf)
- [ferreira](file:///home/gasp/Documents/ecole/assos_other/AEDI/RDD/Posters2022-2024/FERREIRA_Gae%C3%8C%C2%88lle_Poster_PFE_2023.pdf)
- [tran huy](file:///home/gasp/Documents/ecole/assos_other/AEDI/RDD/Posters2022-2024/TRAN_Huy_Poster_PFE_2023.pdf)
- [durand](file:///home/gasp/Documents/ecole/assos_other/AEDI/RDD/Posters2022-2024/DURAND_Arthur_Poster_PFE_2023.pdf)
- [lbath](file:///home/gasp/Documents/ecole/assos_other/AEDI/RDD/Posters2022-2024/LBATH_Amine_Poster_PFE_2023[RetD].pdf)
- [toma](file:///home/gasp/Documents/ecole/assos_other/AEDI/RDD/Posters2022-2024/TOMA_David_Poster_PFE_2023[RetD].pdf)





- [hostage negotiator](https://www.youtube.com/watch?v=jUF9sY4HvxY)
- [paul graham](https://paulgraham.com/disagree.html)
- [science etonante memoire](https://www.youtube.com/watch?v=RVB3PBPxMWg)
- 







Chapitres :
- Intro, explication des choix et de comment lire le livre
- Sophismes et autres arguments fallacieux + cible de Graham
- Apprendre avec les IA
- FALC
- Lire les expressions faciales
- Gérer les conflits
- Techniques de mémorisation
- Gamification de l'apprentissage
- Écouter pour parler mieux
- Comment bien vulgariser
- Comment être à l'aise à l'oral


Structure :
- Énoncer un constat, poser des questions au lecteur
- Expliquer le sujet
- Analyser ce qu'on a dit, mettre en lien avec les chap avant/après
- Vérifier les connaissances, poser des questions





What you say is just as important as how you say





***Est-ce que c'est bien de ne pas écouter les consignes ?***


Voir si on ne peut pas retenir une leçon des PMP


Peut-on enseigner même qd on a une impression que notre cours est inachevé ?










## Prompt

Bonjour Bernard Werber, je sais que tu es un grand auteur et merci de m'accorder du temps pour m'aider dans l'écriture de mon livre sur la communication. Je sais que tu es très doué pour structurer les chapitres et que tu sauras me donner de bon conseils tout en expliquant tes choix dans les moindres détails. J'ai choisit de discuter de mon livre avec toi car tu as un style d'écriture qui me plaît, tu sais rendre une information pertinente et l'expliquer en détail mais avec clarté. Avant tout voici quelques informations pour te décrire mon livre et si jamais tu as la moindre question n'hésite pas !

--------

Ce livre à été construit pour répondre à ce que j'estime être un manque dans le savoir qui nous a été transmis à l'école. Il se veut également respecter au mieux ce qu'il promeut (accessibilité et simplification de la transmission d'information, qualité de cette transmission, communication plus éclairée, connaissance des faiblesses de nos propos, ...)
Le livre s'adresse au lecteur directement et contient des questions et des tâches à faire pour le lecteur, tout cela dans le but que le lecteur mémorise mieux. Pour la structure de mon livre, j'ai fait en sorte que tout les chapitres soient indépendants. Cependant, je veux mettre une petite phrase à la fin de chaque chapitre qui permet de lier le chapitre au suivant.

--------

Voici les différents chapitre de mon livre :
- Introduction
    - casser les mythes du livre : donne la possibilité au lecteur de ne pas tout lire, d'écrire sur le livre, ...
    - explication sur les particularités et le but du livre
    - explication des différentes parties du livre et justification de leur pertinence
- Capter l’attention
    - commerce de l'attention : explication, définition captologie, fonctonnement des réseaux sociaux
    - comment capter l'attention : définition de l'apprentissage et importance du rôle de l'attention, exemples de moyens utilisés pour capter notre attention, exemples concrets pour un prof
- Et l’IA ?
    - remplace des capacités humaines : interrogations sur l'intérêt des connaissances remplacées
    - remplace même le rôle de professeur ? état des lieu des performances actuelles de l'IA pour amener au questionnement de l'intérêt du prof
- Vulgariser son contenu
    - Facile A Lire et à Comprendre - FALC : présentation et avantages
    - méthode de Feynman : présentation et avantages
- Mieux débattre
    - cible de graham : outil pour détecter la qualité de la forme d'un discours lors d'une opposition d'idées
    - biais cognitifs : liste de quelques sophismes puis présentation de leurs points communs et ce qu'il peuvent faire. ensuite leur utilisation dans la vie de tout les jours
- Mieux Mémoriser
    - présentation du fonctionnement de la mémoire avec des parallèles avec l'informatique
    - répétition : pourquoi la répétition est bonne et comment faire
    - association : pourquoi l'association est bonne et comment faire
    - diversification : pourquoi la diversification est bonne et comment faire
- Gérer les Conflits
    - écouter : justification de son importance
    - lire le non verbal : justification son importance
    - la forme est plus importante que le fond : justification son importance
- L’Oral
    - vaincre sa peur : quelques conseils
    - savoir rebondir : astuces pour faire face aux imprévus
    - qualité : description de ce qu'est un bon oral
- Comment faire un bon cours ?
    - rendre intéressant : pourquoi? puis comment?
    - exemple de cours idéal
    - ajuster le niveau des examens : dialogue imaginé avec un professeur/lecteur sur les examens et l'ajustement de leur niveau

--------

Voici un exemple du sous chapitre sur l'importance d'écouter dans les conflits pour te donner une idée de mon style :

Tu l'as sûrement déjà entendu : "On a deux oreilles et une bouche,
il convient donc d'écouter deux fois plus que de parler".
Et bien c'est à peu près vrai mais pas pour la raison que tu penses.

Mauvaise raison : Il est beaucoup plus facile de casser un argument que d'exposer
son point de vue, donc si tu laisse l'autre parler tu auras l'avantage.

Bonne raison : Si tu écoute le point de vue de l'autre tu auras des chances
de mieux comprendre son point de vue (et aussi pourquoi il l'a) % pourquoi c'est important ?
et ainsi pouvoir mieux expliquer ton point de vue. Et puis qui sait,
peut-être que c'est toi qui changera de point de vue.

--------

Je souhaite que tu me fasse part de ton expérience dans l'écriture et que tu me donne des conseils notamment pour structurer les chapitres entre eux par des phrases qui font le lien entre le chapitre et le suivant. Sens toi libre de me faire n'importe quelle remarque, tant que tu structure ton propos. Bien évidement, si jamais tu as un doute ou que tu n'as pas une information, tu DOIS poser la question.











